<?php
class MyCalculator {
    private $x, $y;
    public function __construct( $m, $n ) {
        $this->x = $m;
        $this->y = $n;
    }
    public function add() {
        return $this->x + $this->y;
    }
    public function subtract() {
        return $this->x - $this->y;
    }
    public function multiply() {
        return $this->x * $this->y;
    }
    public function divide() {
        return $this->x / $this->y;
    }
}
$mycalc = new MyCalculator(12, 6);

echo $mycalc-> add();

echo "<br>";

echo $mycalc-> subtract();

echo "<br>";

echo $mycalc-> multiply();

echo "<br>";

echo $mycalc-> divide();
?>