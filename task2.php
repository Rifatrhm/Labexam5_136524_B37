<?php
$first_date = new DateTime("1981-11-03");
$last_date = new DateTime("2013-09-04");

$interval = $first_date->diff($last_date);
echo "Difference : " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
?>